(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-content-pages-content-pages-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/coming-soon/coming-soon-page.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/coming-soon/coming-soon-page.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Coming soon starts-->\n<section id=\"coming-soon\">\n  <div class=\"container-fluid white\">\n    <div class=\"row full-height-vh\">\n      <div class=\"col-12 d-flex align-items-center justify-content-center\">\n        <div class=\"card box-shadow-0 no-border\">\n          <div class=\"card-header text-center\">\n            <h4 class=\"card-title text-dark\">\n              WE ARE LAUNCHING SOON.\n            </h4>\n          </div>\n          <div class=\"card-content\">\n            <div class=\"text-center\">\n              <div class=\"row\">\n                <div class=\"col-sm-12\">\n                  <img alt=\"avtar\" class=\"img-fluid mb-2\" src=\"../../../../assets/img/gallery/coming-soon.png\" width=\"200\">\n                </div>\n              </div>\n              <div id=\"clockFlat\" class=\"getting-started pt-1 mt-2\">\n                <div class=\"px-3 py-3 mr-3 mb-3 d-inline-block\"> <span class=\"text-dark\">{{countdown.days}}</span> <br>\n                  <p class=\"lead mt-2 mb-0 text-dark\"> Days </p>\n                </div>\n                <div class=\"px-3 py-3 mr-3 mb-3 d-inline-block\"> <span class=\"text-dark\">{{countdown.hours}}</span> <br>\n                  <p class=\"lead mt-2 mb-0 text-dark\"> Hours </p>\n                </div>\n                <div class=\"px-3 py-3 mr-3 mb-3 d-inline-block\"> <span class=\"text-dark\">{{countdown.minutes}}</span>\n                  <br>\n                  <p class=\"lead mt-2 mb-0 text-dark\"> Minutes </p>\n                </div>\n                <div class=\"px-2 py-3 mr-3 mb-3 d-inline-block\"> <span class=\"text-dark\">{{countdown.seconds}}</span>\n                  <br>\n                  <p class=\"lead mt-2 mb-0 text-dark\"> Seconds </p>\n                </div>\n              </div>\n              <div class=\"cs-text-divider\">\n                <hr class=\"float-left\"> <span class=\"text-dark\">Subscribe</span>\n                <hr class=\"float-right\">\n              </div>\n              <p class=\"mt-3 text-dark\">\n                If you would like to be notified when our app is live, Please subscribe to our mailing list.\n              </p>\n              <div class=\"row mx-auto\">\n                <div class=\"col-sm-12 mx-auto\">\n                  <input type=\"text\" class=\"form-control mt-3\" placeholder=\"Email\" />\n                  <button class=\"btn btn-lg btn-primary mt-2\">Subscribe</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Coming soon ends-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/error/error-page.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/error/error-page.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Error page starts-->\n<section id=\"error\">\n  <div class=\"container-fluid forgot-password-bg\">\n    <div class=\"row full-height-vh\">\n      <div class=\"col-12 d-flex align-items-center justify-content-center\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 text-center\">\n            <img src=\"../../assets/img/gallery/error.png\" alt=\"\" class=\"img-fluid error-img mt-2\" height=\"300\" width=\"400\">\n            <h1 class=\"text-white mt-4\">404 - Page Not Found!</h1>\n            <div class=\"error-text w-75 mx-auto mt-4\">\n              <p class=\"text-white\">paraphonic unassessable foramination Caulopteris worral Spirophyton\n                encrimson esparcet aggerate chondrule restate whistler shallopy biosystematy area\n                bertram plotting unstarting.</p>\n            </div>\n            <button class=\"btn btn-primary btn-lg mt-3\"><a [routerLink]=\"['/']\" class=\"text-decoration-none text-white\">Back\n                To Home</a></button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Error page ends-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/forgot-password/forgot-password-page.component.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/forgot-password/forgot-password-page.component.html ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Forgot Password Starts-->\n<section id=\"forgot-password\">\n  <div class=\"container-fluid forgot-password-bg\">\n    <div\n      class=\"row full-height-vh m-0  d-flex align-items-center justify-content-center\"\n    >\n      <div class=\"col-md-7 col-sm-12\">\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-body fg-image\">\n              <div class=\"row m-0\">\n                <div class=\"col-lg-6 d-none d-lg-block text-center py-3\">\n                  <img\n                    src=\"../../../../assets/img/gallery/forgot.png\"\n                    alt=\"\"\n                    class=\"img-fluid\"\n                    width=\"300\"\n                    height=\"230\"\n                  />\n                </div>\n                <div class=\"col-lg-6 col-md-12 bg-white px-4 pt-3\">\n                  <h4 class=\"card-title mb-2\">Recover Password</h4>\n                  <p class=\"card-text mb-3\">\n                    Please enter your email address and we'll send you\n                    instructions on how to reset your password.\n                  </p>\n                  <input\n                    type=\"text\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Email\"\n                  />\n                  <div class=\"fg-actions d-flex justify-content-between\">\n                    <div class=\"login-btn\">\n                      <button class=\"btn btn-outline-primary\">\n                        <a\n                          class=\"text-decoration-none\"\n                          [routerLink]=\"['/pages/login']\"\n                          >Back To Login</a\n                        >\n                      </button>\n                    </div>\n                    <div class=\"recover-pass\">\n                      <button class=\"btn btn-primary\">\n                        <a\n                          class=\"text-decoration-none text-white\"\n                          [routerLink]=\"['/pages/forgotpassword']\"\n                        >\n                          Recover\n                        </a>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Forgot Password Ends-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/lock-screen/lock-screen-page.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/lock-screen/lock-screen-page.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Lock Screen Starts-->\n<section id=\"lock-screen\">\n  <div class=\"container-fluid forgot-password-bg\">\n    <div class=\"row full-height-vh m-0\">\n      <div class=\"col-12 d-flex align-items-center justify-content-center\">\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-body lock-screen-img\">\n              <div class=\"row m-0\">\n                <div class=\"col-lg-6 d-lg-block d-none text-center py-2\">\n                  <img\n                    src=\"../../assets/img/gallery/lock.png\"\n                    alt=\"\"\n                    class=\"img-fluid\"\n                    height=\"230\"\n                    width=\"400\"\n                  />\n                </div>\n                <div class=\"col-lg-6 col-md-12 pt-3 px-4 bg-white\">\n                  <h4 class=\"card-title mb-3\">\n                    Your Session is locked\n                  </h4>\n                  <input\n                    type=\"text\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Email\"\n                  />\n                  <input\n                    type=\"password\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"password\"\n                  />\n                  <div class=\"fg-actions d-flex justify-content-between\">\n                    <div class=\"login-btn\">\n                      <button\n                        class=\"btn btn-link text-decoration-none text-primary\"\n                      >\n                        Are You Not John Doe ?\n                      </button>\n                    </div>\n                    <div class=\"recover-pass\">\n                      <button class=\"btn btn-primary\">\n                        <a\n                          class=\"text-decoration-none text-white\"\n                          [routerLink]=\"['/dashboard/dashboard1']\"\n                        >\n                          Unlock\n                        </a>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Lock Screen Ends-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/login/login-page.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/login/login-page.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Login Page Starts-->\n<section id=\"login\">\n  <div class=\"container-fluid\">\n    <div class=\"row full-height-vh m-0\">\n      <div class=\"col-12 d-flex align-items-center justify-content-center\">\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-body login-img\">\n              <div class=\"row m-0\">\n                <div\n                  class=\"col-lg-6 d-lg-block d-none py-2 px-3 text-center align-middle\"\n                >\n                  <img\n                    src=\"../../assets/img/gallery/login.png\"\n                    alt=\"\"\n                    class=\"img-fluid mt-5\"\n                    width=\"400\"\n                    height=\"230\"\n                  />\n                </div>\n                <div class=\"col-lg-6 col-md-12 bg-white px-4 pt-3\">\n                  <h4 class=\"card-title mb-2\">Login</h4>\n                  <p class=\"card-text mb-3\">\n                    Welcome back, please login to your account.\n                  </p>\n                  <input\n                    type=\"text\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Username\"\n                  />\n                  <input\n                    type=\"password\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Password\"\n                  />\n                  <div class=\"d-flex justify-content-between mt-2\">\n                    <div class=\"remember-me\">\n                      <div\n                        class=\"custom-control custom-checkbox custom-control-inline mb-3\"\n                      >\n                        <input\n                          type=\"checkbox\"\n                          id=\"customCheckboxInline1\"\n                          name=\"customCheckboxInline1\"\n                          class=\"custom-control-input\"\n                        />\n                        <label\n                          class=\"custom-control-label\"\n                          for=\"customCheckboxInline1\"\n                        >\n                          Remember Me\n                        </label>\n                      </div>\n                    </div>\n                    <div class=\"forgot-password-option\">\n                      <a\n                        [routerLink]=\"['/pages/forgotpassword']\"\n                        class=\"text-decoration-none text-primary\"\n                        >Forgot Password ?</a\n                      >\n                    </div>\n                  </div>\n                  <div class=\"fg-actions d-flex justify-content-between\">\n                    <div class=\"login-btn\">\n                      <button class=\"btn btn-outline-primary\">\n                        <a\n                          [routerLink]=\"['/pages/register']\"\n                          class=\"text-decoration-none\"\n                          >Register</a\n                        >\n                      </button>\n                    </div>\n                    <div class=\"recover-pass\">\n                      <button class=\"btn btn-primary\">\n                        <a\n                          [routerLink]=\"['/pages/login']\"\n                          class=\"text-decoration-none text-white\"\n                          >Login</a\n                        >\n                      </button>\n                    </div>\n                  </div>\n                  <hr class=\"m-0\" />\n                  <div class=\"d-flex justify-content-between mt-3\">\n                    <div class=\"option-login\">\n                      <h6 class=\"text-decoration-none text-primary\">\n                        Or Login With\n                      </h6>\n                    </div>\n                    <div class=\"social-login-options\">\n                      <a class=\"btn btn-social-icon mr-2 btn-facebook\">\n                        <span class=\"fa fa-facebook\"></span>\n                      </a>\n                      <a class=\"btn btn-social-icon mr-2 btn-twitter\">\n                        <span class=\"fa fa-twitter\"></span>\n                      </a>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Login Page Ends-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/maintenance/maintenance-page.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/maintenance/maintenance-page.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Under Maintenance Starts-->\n<section id=\"maintenance\" class=\"full-height-vh\">\n  <div class=\"container-fluid\">\n    <div class=\"row full-height-vh\">\n      <div class=\"col-12 d-flex align-items-center justify-content-center\">\n        <div class=\"row\">\n          <div class=\"col-sm-12 text-center\">\n            <img src=\"../../../../assets/img/gallery/maintenance.png\" alt=\"\" class=\"img-fluid maintenance-img mt-2\"\n              height=\"300\" width=\"400\">\n            <h1 class=\"text-white mt-4\">Under Maintenance!</h1>\n            <div class=\"w-75 mx-auto maintenance-text mt-3\">\n              <p class=\"text-white\">paraphonic unassessable foramination Caulopteris worral Spirophyton\n                encrimson esparcet aggerate chondrule restate whistler shallopy biosystematy area\n                bertram plotting unstarting quarterstaff.\n              </p>\n            </div>\n            <button class=\"btn btn-primary btn-lg mt-4\"><a href=\"dashboard1.html\" class=\"text-decoration-none text-white\">Back\n                To Home</a></button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Under Maintenance Starts-->\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/register/register-page.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/content-pages/register/register-page.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--Registration Page Starts-->\n<section id=\"regestration\">\n  <div class=\"container-fluid\">\n    <div class=\"row full-height-vh m-0\">\n      <div class=\"col-12 d-flex align-items-center justify-content-center\">\n        <div class=\"card\">\n          <div class=\"card-content\">\n            <div class=\"card-body register-img\">\n              <div class=\"row m-0\">\n                <div class=\"col-lg-6 d-none d-lg-block py-2 px-3 text-center\">\n                  <img\n                    src=\"../../assets/img/gallery/register.png\"\n                    alt=\"\"\n                    class=\"img-fluid mt-3 pl-3\"\n                    width=\"400\"\n                    height=\"230\"\n                  />\n                </div>\n                <div class=\"col-lg-6 col-md-12 pt-3 px-4 bg-white\">\n                  <h4 class=\"card-title mb-2\">Create Account</h4>\n                  <p class=\"card-text mb-3\">\n                    Fill the below form to create a new account.\n                  </p>\n                  <input\n                    type=\"text\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Name\"\n                  />\n                  <input\n                    type=\"email\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Email\"\n                  />\n                  <input\n                    type=\"password\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Password\"\n                  />\n                  <input\n                    type=\"password\"\n                    class=\"form-control mb-3\"\n                    placeholder=\"Confirm Password\"\n                  />\n                  <div\n                    class=\"custom-control custom-checkbox custom-control-inline mb-3\"\n                  >\n                    <input\n                      type=\"checkbox\"\n                      id=\"customCheckboxInline1\"\n                      name=\"customCheckboxInline1\"\n                      class=\"custom-control-input\"\n                      checked\n                    />\n                    <label\n                      class=\"custom-control-label\"\n                      for=\"customCheckboxInline1\"\n                    >\n                      I accept the terms & conditions.\n                    </label>\n                  </div>\n                  <div class=\"fg-actions d-flex justify-content-between\">\n                    <div class=\"login-btn\">\n                      <button class=\"btn btn-outline-primary\">\n                        <a\n                          href=\"login-page.html\"\n                          class=\"text-decoration-none\"\n                          [routerLink]=\"['/pages/login']\"\n                        >\n                          Back To Login\n                        </a>\n                      </button>\n                    </div>\n                    <div class=\"recover-pass\">\n                      <button class=\"btn btn-primary\">\n                        <a class=\"text-decoration-none text-white\">\n                          Register\n                        </a>\n                      </button>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n<!--Registration Page Ends-->\n"

/***/ }),

/***/ "./src/app/pages/content-pages/coming-soon/coming-soon-page.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/content-pages/coming-soon/coming-soon-page.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#coming-soon {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n  #coming-soon .card-coming-soon {\n    border-radius: 0.5rem; }\n  #coming-soon .getting-started {\n    display: flex;\n    flex-wrap: wrap;\n    justify-content: center; }\n  #coming-soon .cs-text-divider {\n    width: 100%;\n    text-align: center; }\n  #coming-soon .cs-text-divider hr {\n      margin-left: auto;\n      margin-right: auto;\n      width: 40%; }\n  #coming-soon .cs-text-divider span {\n      position: relative;\n      top: 11px; }\n  #coming-soon .btn.btn-lg.btn-primary {\n    color: #fff !important; }\n  @media (max-width: 540px) {\n  #coming-soon {\n    overflow: hidden; }\n    #coming-soon .card-coming-soon {\n      position: relative;\n      top: -12px; }\n      #coming-soon .card-coming-soon .card-header {\n        padding: 1rem; }\n        #coming-soon .card-coming-soon .card-header h4.card-title {\n          font-size: 1rem; }\n      #coming-soon .card-coming-soon img.img-cs {\n        width: 100px; }\n      #coming-soon .card-coming-soon .cs-text-divider hr {\n        margin-left: auto;\n        margin-right: auto;\n        width: 35%; }\n      #coming-soon .card-coming-soon .getting-started {\n        padding-top: 0 !important;\n        margin-top: auto !important; }\n        #coming-soon .card-coming-soon .getting-started .clockCard {\n          margin-bottom: 0.5rem !important; }\n          #coming-soon .card-coming-soon .getting-started .clockCard P.lead {\n            margin-top: 0 !important; }\n      #coming-soon .card-coming-soon input.form-control {\n        margin-top: 0.5rem !important; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9jb21pbmctc29vbi9EOlxcQW5ndWxhclxcYmFja29mZmljZS9zcmNcXGFwcFxccGFnZXNcXGNvbnRlbnQtcGFnZXNcXGNvbWluZy1zb29uXFxjb21pbmctc29vbi1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTtFQUg5QjtJQU1JLHFCQUFxQixFQUFBO0VBTnpCO0lBVUksYUFBYTtJQUNiLGVBQWU7SUFDZix1QkFBdUIsRUFBQTtFQVozQjtJQWdCSSxXQUFXO0lBQ1gsa0JBQWtCLEVBQUE7RUFqQnRCO01Bb0JNLGlCQUFpQjtNQUNqQixrQkFBa0I7TUFDbEIsVUFBVSxFQUFBO0VBdEJoQjtNQTBCTSxrQkFBa0I7TUFDbEIsU0FBUyxFQUFBO0VBM0JmO0lBZ0NJLHNCQUFzQixFQUFBO0VBSzFCO0VBQ0U7SUFDRSxnQkFBZ0IsRUFBQTtJQURsQjtNQUlJLGtCQUFrQjtNQUNsQixVQUFVLEVBQUE7TUFMZDtRQVFNLGFBQWEsRUFBQTtRQVJuQjtVQVdRLGVBQWUsRUFBQTtNQVh2QjtRQWdCTSxZQUFZLEVBQUE7TUFoQmxCO1FBc0JRLGlCQUFpQjtRQUNqQixrQkFBa0I7UUFDbEIsVUFBVSxFQUFBO01BeEJsQjtRQTZCTSx5QkFBeUI7UUFDekIsMkJBQTJCLEVBQUE7UUE5QmpDO1VBaUNRLGdDQUFnQyxFQUFBO1VBakN4QztZQW9DVSx3QkFBd0IsRUFBQTtNQXBDbEM7UUEwQ00sNkJBQTZCLEVBQUEsRUFDOUIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jb250ZW50LXBhZ2VzL2NvbWluZy1zb29uL2NvbWluZy1zb29uLXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDb21pbmcgU29vblxuXG4jY29taW5nLXNvb24ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWcvZ2FsbGVyeS9saWdodC1iZy5qcGcnKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5cbiAgLmNhcmQtY29taW5nLXNvb24ge1xuICAgIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgfVxuXG4gIC5nZXR0aW5nLXN0YXJ0ZWQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG5cbiAgLmNzLXRleHQtZGl2aWRlciB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgaHIge1xuICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgICB3aWR0aDogNDAlO1xuICAgIH1cblxuICAgIHNwYW4ge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgdG9wOiAxMXB4O1xuICAgIH1cbiAgfVxuXG4gIC5idG4uYnRuLWxnLmJ0bi1wcmltYXJ5IHtcbiAgICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xuICB9XG59XG5cblxuQG1lZGlhKG1heC13aWR0aDo1NDBweCkge1xuICAjY29taW5nLXNvb24ge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICAuY2FyZC1jb21pbmctc29vbiB7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICB0b3A6IC0xMnB4O1xuXG4gICAgICAuY2FyZC1oZWFkZXIge1xuICAgICAgICBwYWRkaW5nOiAxcmVtO1xuXG4gICAgICAgIGg0LmNhcmQtdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpbWcuaW1nLWNzIHtcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xuICAgICAgfVxuXG4gICAgICAuY3MtdGV4dC1kaXZpZGVyIHtcblxuICAgICAgICBociB7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgICAgICAgIHdpZHRoOiAzNSU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLmdldHRpbmctc3RhcnRlZCB7XG4gICAgICAgIHBhZGRpbmctdG9wOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi10b3A6IGF1dG8gIWltcG9ydGFudDtcblxuICAgICAgICAuY2xvY2tDYXJkIHtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcblxuICAgICAgICAgIFAubGVhZCB7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlucHV0LmZvcm0tY29udHJvbCB7XG4gICAgICAgIG1hcmdpbi10b3A6IDAuNXJlbSAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgIH1cblxuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/content-pages/coming-soon/coming-soon-page.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/content-pages/coming-soon/coming-soon-page.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ComingSoonPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComingSoonPageComponent", function() { return ComingSoonPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");






var ComingSoonPageComponent = /** @class */ (function () {
    function ComingSoonPageComponent() {
        this.pickDate = new Date().setMonth(new Date().getMonth() + 2);
        this.datePipe = new _angular_common__WEBPACK_IMPORTED_MODULE_5__["DatePipe"]('en-US');
        this.launchDate = this.datePipe.transform(this.pickDate, 'yyyy-MM-dd');
        // Set the defaults
        this.countdown = {
            weeks: '',
            days: '',
            hours: '',
            minutes: '',
            seconds: ''
        };
        // Set the private defaults
        this._unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    ComingSoonPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        var currDate = moment__WEBPACK_IMPORTED_MODULE_4__();
        var launchDate = moment__WEBPACK_IMPORTED_MODULE_4__(this.launchDate);
        var diff = launchDate.diff(currDate, 'seconds');
        this.countdown = this.calculateRemainingTime(diff);
        // Create a subscribable interval
        var countDown = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["interval"])(1000)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (value) {
            return diff = diff - 1;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (value) {
            return _this.calculateRemainingTime(value);
        }));
        // Subscribe to the countdown interval
        countDown
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this._unsubscribeAll))
            .subscribe(function (value) {
            _this.countdown = value;
        });
    };
    ComingSoonPageComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    ComingSoonPageComponent.prototype.calculateRemainingTime = function (seconds) {
        var timeLeft = moment__WEBPACK_IMPORTED_MODULE_4__["duration"](seconds, 'seconds');
        return {
            weeks: timeLeft.asWeeks().toFixed(0),
            days: timeLeft.asDays().toFixed(0),
            hours: timeLeft.hours(),
            minutes: timeLeft.minutes(),
            seconds: timeLeft.seconds()
        };
    };
    ComingSoonPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-coming-soon-page',
            template: __webpack_require__(/*! raw-loader!./coming-soon-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/coming-soon/coming-soon-page.component.html"),
            styles: [__webpack_require__(/*! ./coming-soon-page.component.scss */ "./src/app/pages/content-pages/coming-soon/coming-soon-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ComingSoonPageComponent);
    return ComingSoonPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/content-pages-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/content-pages/content-pages-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ContentPagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentPagesRoutingModule", function() { return ContentPagesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _coming_soon_coming_soon_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./coming-soon/coming-soon-page.component */ "./src/app/pages/content-pages/coming-soon/coming-soon-page.component.ts");
/* harmony import */ var _error_error_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./error/error-page.component */ "./src/app/pages/content-pages/error/error-page.component.ts");
/* harmony import */ var _forgot_password_forgot_password_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forgot-password/forgot-password-page.component */ "./src/app/pages/content-pages/forgot-password/forgot-password-page.component.ts");
/* harmony import */ var _lock_screen_lock_screen_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./lock-screen/lock-screen-page.component */ "./src/app/pages/content-pages/lock-screen/lock-screen-page.component.ts");
/* harmony import */ var _login_login_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/login-page.component */ "./src/app/pages/content-pages/login/login-page.component.ts");
/* harmony import */ var _maintenance_maintenance_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./maintenance/maintenance-page.component */ "./src/app/pages/content-pages/maintenance/maintenance-page.component.ts");
/* harmony import */ var _register_register_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./register/register-page.component */ "./src/app/pages/content-pages/register/register-page.component.ts");










var routes = [
    {
        path: '',
        children: [
            {
                path: 'comingsoon',
                component: _coming_soon_coming_soon_page_component__WEBPACK_IMPORTED_MODULE_3__["ComingSoonPageComponent"],
                data: {
                    title: 'Coming Soon page'
                }
            },
            {
                path: 'error',
                component: _error_error_page_component__WEBPACK_IMPORTED_MODULE_4__["ErrorPageComponent"],
                data: {
                    title: 'Error Page'
                }
            },
            {
                path: 'forgotpassword',
                component: _forgot_password_forgot_password_page_component__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordPageComponent"],
                data: {
                    title: 'Forgot Password Page'
                }
            },
            {
                path: 'lockscreen',
                component: _lock_screen_lock_screen_page_component__WEBPACK_IMPORTED_MODULE_6__["LockScreenPageComponent"],
                data: {
                    title: 'Lock Screen page'
                }
            },
            {
                path: 'login',
                component: _login_login_page_component__WEBPACK_IMPORTED_MODULE_7__["LoginPageComponent"],
                data: {
                    title: 'Login Page'
                }
            },
            {
                path: 'maintenance',
                component: _maintenance_maintenance_page_component__WEBPACK_IMPORTED_MODULE_8__["MaintenancePageComponent"],
                data: {
                    title: 'Maintenance Page'
                }
            },
            {
                path: 'register',
                component: _register_register_page_component__WEBPACK_IMPORTED_MODULE_9__["RegisterPageComponent"],
                data: {
                    title: 'Register Page'
                }
            }
        ]
    }
];
var ContentPagesRoutingModule = /** @class */ (function () {
    function ContentPagesRoutingModule() {
    }
    ContentPagesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ContentPagesRoutingModule);
    return ContentPagesRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/content-pages.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/content-pages/content-pages.module.ts ***!
  \*************************************************************/
/*! exports provided: ContentPagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContentPagesModule", function() { return ContentPagesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _content_pages_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./content-pages-routing.module */ "./src/app/pages/content-pages/content-pages-routing.module.ts");
/* harmony import */ var _coming_soon_coming_soon_page_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./coming-soon/coming-soon-page.component */ "./src/app/pages/content-pages/coming-soon/coming-soon-page.component.ts");
/* harmony import */ var _error_error_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./error/error-page.component */ "./src/app/pages/content-pages/error/error-page.component.ts");
/* harmony import */ var _forgot_password_forgot_password_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./forgot-password/forgot-password-page.component */ "./src/app/pages/content-pages/forgot-password/forgot-password-page.component.ts");
/* harmony import */ var _lock_screen_lock_screen_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./lock-screen/lock-screen-page.component */ "./src/app/pages/content-pages/lock-screen/lock-screen-page.component.ts");
/* harmony import */ var _login_login_page_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/login-page.component */ "./src/app/pages/content-pages/login/login-page.component.ts");
/* harmony import */ var _maintenance_maintenance_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./maintenance/maintenance-page.component */ "./src/app/pages/content-pages/maintenance/maintenance-page.component.ts");
/* harmony import */ var _register_register_page_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./register/register-page.component */ "./src/app/pages/content-pages/register/register-page.component.ts");












var ContentPagesModule = /** @class */ (function () {
    function ContentPagesModule() {
    }
    ContentPagesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _content_pages_routing_module__WEBPACK_IMPORTED_MODULE_4__["ContentPagesRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
            ],
            declarations: [
                _coming_soon_coming_soon_page_component__WEBPACK_IMPORTED_MODULE_5__["ComingSoonPageComponent"],
                _error_error_page_component__WEBPACK_IMPORTED_MODULE_6__["ErrorPageComponent"],
                _forgot_password_forgot_password_page_component__WEBPACK_IMPORTED_MODULE_7__["ForgotPasswordPageComponent"],
                _lock_screen_lock_screen_page_component__WEBPACK_IMPORTED_MODULE_8__["LockScreenPageComponent"],
                _login_login_page_component__WEBPACK_IMPORTED_MODULE_9__["LoginPageComponent"],
                _maintenance_maintenance_page_component__WEBPACK_IMPORTED_MODULE_10__["MaintenancePageComponent"],
                _register_register_page_component__WEBPACK_IMPORTED_MODULE_11__["RegisterPageComponent"]
            ]
        })
    ], ContentPagesModule);
    return ContentPagesModule;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/error/error-page.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/content-pages/error/error-page.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#error {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n\n@media (max-width: 768px) {\n  #error .error-img {\n    width: 300px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9lcnJvci9EOlxcQW5ndWxhclxcYmFja29mZmljZS9zcmNcXGFwcFxccGFnZXNcXGNvbnRlbnQtcGFnZXNcXGVycm9yXFxlcnJvci1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTs7QUFHOUI7RUFDRTtJQUVJLFlBQVksRUFBQSxFQUNiIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9lcnJvci9lcnJvci1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gRXJyb3JcbiNlcnJvciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9nYWxsZXJ5L2xpZ2h0LWJnLmpwZycpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuQG1lZGlhKG1heC13aWR0aDo3NjhweCkge1xuICAjZXJyb3Ige1xuICAgIC5lcnJvci1pbWcge1xuICAgICAgd2lkdGg6IDMwMHB4O1xuICAgIH1cbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/content-pages/error/error-page.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/content-pages/error/error-page.component.ts ***!
  \*******************************************************************/
/*! exports provided: ErrorPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorPageComponent", function() { return ErrorPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ErrorPageComponent = /** @class */ (function () {
    function ErrorPageComponent() {
    }
    ErrorPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error-page',
            template: __webpack_require__(/*! raw-loader!./error-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/error/error-page.component.html"),
            styles: [__webpack_require__(/*! ./error-page.component.scss */ "./src/app/pages/content-pages/error/error-page.component.scss")]
        })
    ], ErrorPageComponent);
    return ErrorPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/forgot-password/forgot-password-page.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/content-pages/forgot-password/forgot-password-page.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#forgot-password .forgot-password-bg {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n\n#forgot-password .fg-image {\n  padding: 0;\n  background: #f0f0f0; }\n\n#forgot-password .login-btn .btn.btn-outline-primary:hover a {\n  color: #fff; }\n\n#forgot-password .recover-pass button.btn.btn-primary {\n  color: #fff !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9mb3Jnb3QtcGFzc3dvcmQvRDpcXEFuZ3VsYXJcXGJhY2tvZmZpY2Uvc3JjXFxhcHBcXHBhZ2VzXFxjb250ZW50LXBhZ2VzXFxmb3Jnb3QtcGFzc3dvcmRcXGZvcmdvdC1wYXNzd29yZC1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBRUkscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTs7QUFKaEM7RUFRSSxVQUFVO0VBQ1YsbUJBQW1CLEVBQUE7O0FBVHZCO0VBaUJVLFdBQVcsRUFBQTs7QUFqQnJCO0VBeUJNLHNCQUFzQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9mb3Jnb3QtcGFzc3dvcmQvZm9yZ290LXBhc3N3b3JkLXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBGb3Jnb3QgUGFzc3dvcmRcblxuI2ZvcmdvdC1wYXNzd29yZCB7XG4gIC5mb3Jnb3QtcGFzc3dvcmQtYmcge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9pbWcvZ2FsbGVyeS9saWdodC1iZy5qcGdcIik7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgfVxuXG4gIC5mZy1pbWFnZSB7XG4gICAgcGFkZGluZzogMDtcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICB9XG5cblxuICAubG9naW4tYnRuIHtcbiAgICAuYnRuLmJ0bi1vdXRsaW5lLXByaW1hcnkge1xuICAgICAgJjpob3ZlciB7XG4gICAgICAgIGEge1xuICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLnJlY292ZXItcGFzcyB7XG4gICAgYnV0dG9uLmJ0bi5idG4tcHJpbWFyeSB7XG4gICAgICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xuICAgIH1cbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/content-pages/forgot-password/forgot-password-page.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/content-pages/forgot-password/forgot-password-page.component.ts ***!
  \***************************************************************************************/
/*! exports provided: ForgotPasswordPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPageComponent", function() { return ForgotPasswordPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ForgotPasswordPageComponent = /** @class */ (function () {
    function ForgotPasswordPageComponent(router, route) {
        this.router = router;
        this.route = route;
    }
    // On submit click, reset form fields
    ForgotPasswordPageComponent.prototype.onSubmit = function () {
        this.forogtPasswordForm.reset();
    };
    // On login link click
    ForgotPasswordPageComponent.prototype.onLogin = function () {
        this.router.navigate(['login'], { relativeTo: this.route.parent });
    };
    // On registration link click
    ForgotPasswordPageComponent.prototype.onRegister = function () {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    };
    ForgotPasswordPageComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], ForgotPasswordPageComponent.prototype, "forogtPasswordForm", void 0);
    ForgotPasswordPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgot-password-page',
            template: __webpack_require__(/*! raw-loader!./forgot-password-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/forgot-password/forgot-password-page.component.html"),
            styles: [__webpack_require__(/*! ./forgot-password-page.component.scss */ "./src/app/pages/content-pages/forgot-password/forgot-password-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ForgotPasswordPageComponent);
    return ForgotPasswordPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/lock-screen/lock-screen-page.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/content-pages/lock-screen/lock-screen-page.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#lock-screen {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n  #lock-screen .lock-screen-img {\n    background: #f0f0f0;\n    padding: 0; }\n  #lock-screen .btn.btn-link.text-decoration-none:hover {\n    text-decoration: none !important; }\n  #lock-screen .login-btn .btn.btn-link {\n    font-size: 14px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9sb2NrLXNjcmVlbi9EOlxcQW5ndWxhclxcYmFja29mZmljZS9zcmNcXGFwcFxccGFnZXNcXGNvbnRlbnQtcGFnZXNcXGxvY2stc2NyZWVuXFxsb2NrLXNjcmVlbi1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTtFQUg5QjtJQU1JLG1CQUFtQjtJQUNuQixVQUFVLEVBQUE7RUFQZDtJQVlNLGdDQUFnQyxFQUFBO0VBWnRDO0lBa0JNLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRlbnQtcGFnZXMvbG9jay1zY3JlZW4vbG9jay1zY3JlZW4tcGFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEZvcmdvdCBQYXNzd29yZFxuXG4jbG9jay1zY3JlZW4ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1nL2dhbGxlcnkvbGlnaHQtYmcuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcblxuICAubG9jay1zY3JlZW4taW1nIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cblxuICAuYnRuLmJ0bi1saW5rLnRleHQtZGVjb3JhdGlvbi1ub25lIHtcbiAgICAmOmhvdmVyIHtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZSAhaW1wb3J0YW50O1xuICAgIH1cbiAgfVxuXG4gIC5sb2dpbi1idG4ge1xuICAgIC5idG4uYnRuLWxpbmsge1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/content-pages/lock-screen/lock-screen-page.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/content-pages/lock-screen/lock-screen-page.component.ts ***!
  \*******************************************************************************/
/*! exports provided: LockScreenPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LockScreenPageComponent", function() { return LockScreenPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var LockScreenPageComponent = /** @class */ (function () {
    function LockScreenPageComponent() {
    }
    LockScreenPageComponent.prototype.onSubmit = function () {
        this.lockScreenForm.reset();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], LockScreenPageComponent.prototype, "lockScreenForm", void 0);
    LockScreenPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lock-screen-page',
            template: __webpack_require__(/*! raw-loader!./lock-screen-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/lock-screen/lock-screen-page.component.html"),
            styles: [__webpack_require__(/*! ./lock-screen-page.component.scss */ "./src/app/pages/content-pages/lock-screen/lock-screen-page.component.scss")]
        })
    ], LockScreenPageComponent);
    return LockScreenPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/login/login-page.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/content-pages/login/login-page.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#login {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n  #login .login-img {\n    padding: 0;\n    background: #f0f0f0; }\n  #login .login-btn .btn.btn-outline-primary:hover a {\n    color: #fff; }\n  #login .recover-pass button.btn.btn-primary {\n    color: #fff !important; }\n  #login .forgot-password-option a.text-decoration-none,\n  #login .forgot-password-option .custom-control-label,\n  #login .remember-me a.text-decoration-none,\n  #login .remember-me .custom-control-label {\n    font-size: 14px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9sb2dpbi9EOlxcQW5ndWxhclxcYmFja29mZmljZS9zcmNcXGFwcFxccGFnZXNcXGNvbnRlbnQtcGFnZXNcXGxvZ2luXFxsb2dpbi1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTtFQUg5QjtJQU1JLFVBQVU7SUFDVixtQkFBbUIsRUFBQTtFQVB2QjtJQWNVLFdBQVcsRUFBQTtFQWRyQjtJQXNCTSxzQkFBc0IsRUFBQTtFQXRCNUI7Ozs7SUErQk0sZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9sb2dpbi9sb2dpbi1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTG9naW4gUGFnZVxuXG4jbG9naW4ge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWcvZ2FsbGVyeS9saWdodC1iZy5qcGcnKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5cbiAgLmxvZ2luLWltZyB7XG4gICAgcGFkZGluZzogMDtcbiAgICBiYWNrZ3JvdW5kOiAjZjBmMGYwO1xuICB9XG5cbiAgLmxvZ2luLWJ0biB7XG4gICAgLmJ0bi5idG4tb3V0bGluZS1wcmltYXJ5IHtcbiAgICAgICY6aG92ZXIge1xuICAgICAgICBhIHtcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5yZWNvdmVyLXBhc3Mge1xuICAgIGJ1dHRvbi5idG4uYnRuLXByaW1hcnkge1xuICAgICAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbiAgICB9XG4gIH1cblxuICAuZm9yZ290LXBhc3N3b3JkLW9wdGlvbixcbiAgLnJlbWVtYmVyLW1lIHtcblxuICAgIGEudGV4dC1kZWNvcmF0aW9uLW5vbmUsXG4gICAgLmN1c3RvbS1jb250cm9sLWxhYmVsIHtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG4gIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/content-pages/login/login-page.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/content-pages/login/login-page.component.ts ***!
  \*******************************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(router, route) {
        this.router = router;
        this.route = route;
    }
    // On submit button click
    LoginPageComponent.prototype.onSubmit = function () {
        this.loginForm.reset();
    };
    // On Forgot password link click
    LoginPageComponent.prototype.onForgotPassword = function () {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    };
    // On registration link click
    LoginPageComponent.prototype.onRegister = function () {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    };
    LoginPageComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], LoginPageComponent.prototype, "loginForm", void 0);
    LoginPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-page',
            template: __webpack_require__(/*! raw-loader!./login-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/login/login-page.component.html"),
            styles: [__webpack_require__(/*! ./login-page.component.scss */ "./src/app/pages/content-pages/login/login-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/maintenance/maintenance-page.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/content-pages/maintenance/maintenance-page.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#maintenance {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n\n@media (max-width: 768px) {\n  #maintenance .maintenance-img {\n    width: 300px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9tYWludGVuYW5jZS9EOlxcQW5ndWxhclxcYmFja29mZmljZS9zcmNcXGFwcFxccGFnZXNcXGNvbnRlbnQtcGFnZXNcXG1haW50ZW5hbmNlXFxtYWludGVuYW5jZS1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTs7QUFHOUI7RUFDRTtJQUVJLFlBQVksRUFBQSxFQUNiIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9tYWludGVuYW5jZS9tYWludGVuYW5jZS1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTWFpbnRlbmFuY2VcblxuI21haW50ZW5hbmNlIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1nL2dhbGxlcnkvbGlnaHQtYmcuanBnJyk7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuXG5AbWVkaWEobWF4LXdpZHRoOiA3NjhweCkge1xuICAjbWFpbnRlbmFuY2Uge1xuICAgIC5tYWludGVuYW5jZS1pbWcge1xuICAgICAgd2lkdGg6IDMwMHB4O1xuICAgIH1cbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/content-pages/maintenance/maintenance-page.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/content-pages/maintenance/maintenance-page.component.ts ***!
  \*******************************************************************************/
/*! exports provided: MaintenancePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaintenancePageComponent", function() { return MaintenancePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MaintenancePageComponent = /** @class */ (function () {
    function MaintenancePageComponent() {
    }
    MaintenancePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-maintenance-page',
            template: __webpack_require__(/*! raw-loader!./maintenance-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/maintenance/maintenance-page.component.html"),
            styles: [__webpack_require__(/*! ./maintenance-page.component.scss */ "./src/app/pages/content-pages/maintenance/maintenance-page.component.scss")]
        })
    ], MaintenancePageComponent);
    return MaintenancePageComponent;
}());



/***/ }),

/***/ "./src/app/pages/content-pages/register/register-page.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/content-pages/register/register-page.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#regestration {\n  background-image: url('light-bg.jpg');\n  background-size: 100% 100%;\n  background-repeat: no-repeat; }\n  #regestration .register-img {\n    background: #f0f0f0;\n    padding: 0; }\n  #regestration .login-btn .btn.btn-outline-primary:hover a {\n    color: #fff; }\n  #regestration .recover-pass button.btn.btn-primary {\n    color: #fff !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGVudC1wYWdlcy9yZWdpc3Rlci9EOlxcQW5ndWxhclxcYmFja29mZmljZS9zcmNcXGFwcFxccGFnZXNcXGNvbnRlbnQtcGFnZXNcXHJlZ2lzdGVyXFxyZWdpc3Rlci1wYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UscUNBQW9FO0VBQ3BFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQTtFQUg5QjtJQU1JLG1CQUFtQjtJQUNuQixVQUFVLEVBQUE7RUFQZDtJQWNVLFdBQVcsRUFBQTtFQWRyQjtJQXNCTSxzQkFBc0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRlbnQtcGFnZXMvcmVnaXN0ZXIvcmVnaXN0ZXItcGFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEZvciBSZWdpc3RyYXRpb24gcGFnZVxuXG4jcmVnZXN0cmF0aW9uIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9nYWxsZXJ5L2xpZ2h0LWJnLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDEwMCU7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG5cbiAgLnJlZ2lzdGVyLWltZyB7XG4gICAgYmFja2dyb3VuZDogI2YwZjBmMDtcbiAgICBwYWRkaW5nOiAwO1xuICB9XG5cbiAgLmxvZ2luLWJ0biB7XG4gICAgLmJ0bi5idG4tb3V0bGluZS1wcmltYXJ5IHtcbiAgICAgICY6aG92ZXIge1xuICAgICAgICBhIHtcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5yZWNvdmVyLXBhc3Mge1xuICAgIGJ1dHRvbi5idG4uYnRuLXByaW1hcnkge1xuICAgICAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbiAgICB9XG4gIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/content-pages/register/register-page.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/content-pages/register/register-page.component.ts ***!
  \*************************************************************************/
/*! exports provided: RegisterPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageComponent", function() { return RegisterPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var RegisterPageComponent = /** @class */ (function () {
    function RegisterPageComponent() {
    }
    //  On submit click, reset field value
    RegisterPageComponent.prototype.onSubmit = function () {
        this.registerForm.reset();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('f', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], RegisterPageComponent.prototype, "registerForm", void 0);
    RegisterPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register-page',
            template: __webpack_require__(/*! raw-loader!./register-page.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/content-pages/register/register-page.component.html"),
            styles: [__webpack_require__(/*! ./register-page.component.scss */ "./src/app/pages/content-pages/register/register-page.component.scss")]
        })
    ], RegisterPageComponent);
    return RegisterPageComponent;
}());



/***/ })

}]);
//# sourceMappingURL=pages-content-pages-content-pages-module.js.map